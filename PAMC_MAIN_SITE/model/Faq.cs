﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAMC_MAIN_SITE.model
{
    public class Faq
    {
        public string question { get; set; }
        public string answer { get; set; }
        public string usertype { get; set; }
    }
}

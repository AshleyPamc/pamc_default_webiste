﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAMC_MAIN_SITE.model
{
    public class JoinMail
    {
        public string provid { get; set; }
        public string practiceName { get; set; }
        public string contactNumber { get; set; }
        public string emailAddress { get; set; }
    }
}

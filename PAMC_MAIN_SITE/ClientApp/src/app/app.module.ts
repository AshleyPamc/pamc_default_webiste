import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from '@clr/angular';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { WebsitesComponent } from './websites/websites.component';
import { PopiaComponent } from './popia/popia.component';
import { FAQComponent } from './faq/faq.component';
import { ProviderNetworkComponent } from './provider-network/provider-network.component';
import { MemberBenefitsComponent } from './member-benefits/member-benefits.component';
import { HealthAriclesComponent } from './health-aricles/health-aricles.component';

const routes: Routes = [
  { path: 'Home', component: HomeComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'Websites', component: WebsitesComponent },
  { path: 'Health-Articles', component: HealthAriclesComponent },
  { path: 'POPIA', component: PopiaComponent },
  { path: 'ProviderNetwork', component: ProviderNetworkComponent },
  { path: 'MemberBenefits', component: MemberBenefitsComponent },
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
  { path: '**', redirectTo: '/Home' },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    WebsitesComponent,
    PopiaComponent,
    FAQComponent,
    ProviderNetworkComponent,
    MemberBenefitsComponent,
    HealthAriclesComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, {      
      scrollPositionRestoration: 'enabled',
    }),
    ClarityModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-member-benefits',
  templateUrl: './member-benefits.component.html',
  styleUrls: ['./member-benefits.component.css']
})
export class MemberBenefitsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // Scroll to top on load
    var scrollElem = document.querySelector('#top');
    scrollElem.scrollIntoView();


    // Custom accordian
    var acc = document.getElementsByClassName("accordion");
    var i;
        
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
  }

}

import { Injectable } from '@angular/core';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _dataService: DataService) { }

  get DataService(): DataService {
    return this._dataService
  }
}

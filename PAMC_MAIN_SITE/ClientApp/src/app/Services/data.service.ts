import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Faq } from '../models/faq';
import { JoinModel } from '../models/join-model';
import { ResultModel } from '../models/result-model';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  private _faqList: Faq[] = [];
  private _busy: boolean = false;
  private _openFaq: boolean = false;
  private _goToMediVouchSection: boolean = false;
  private _screenWidth: number;

  private _joinDetail: JoinModel = new JoinModel();
  private _joinReqResult: ResultModel;
  private _showMailSentModal: boolean = false;
    
  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, private _router: Router) {

    this.ScreenWidth = window.innerWidth;
  }

  public get faqList(): Faq[] {
    return this._faqList;
  }
  public set faqList(value: Faq[]) {
    this._faqList = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get openFaq(): boolean {
    return this._openFaq;
  }
  public set openFaq(value: boolean) {
    this._openFaq = value;
  }
  public get goToMediVouchSection(): boolean {
    return this._goToMediVouchSection;
  }
  public set goToMediVouchSection(value: boolean) {
    this._goToMediVouchSection = value;
  }
  public get ScreenWidth(): number {
    return this._screenWidth;
  }
  public set ScreenWidth(value: number) {
    this._screenWidth = value;
  }
  public get joinDetail(): JoinModel {
    return this._joinDetail;
  }
  public set joinDetail(value: JoinModel) {
    this._joinDetail = value;
  }
  public get joinReqResult(): ResultModel {
    return this._joinReqResult;
  }
  public set joinReqResult(value: ResultModel) {
    this._joinReqResult = value;
  }
  public get showMailSentModal(): boolean {
    return this._showMailSentModal;
  }
  public set showMailSentModal(value: boolean) {
    this._showMailSentModal = value;
  }
  
  GetFaqs(data: Faq) {

    this.busy = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa('&nUwrLd6ymR#MpcTJqA2:QsNKSL+vkx9(*ZmIbDw!')
      })
    };

    this._http.post(this._baseUrl + 'apiW/Data/GetFaqs', data, httpOptions
    ).subscribe((result: Faq[]) => {
      this.faqList = [];
      this.faqList = result;
    },
      (error) => {
        console.log(error);
      },
      () => {
        this.busy = false;
        this.openFaq = true;

      });

  }

  SendJoinRequest(data: JoinModel) {
        
    this._busy = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa('&nUwrLd6ymR#MpcTJqA2:QsNKSL+vkx9(*ZmIbDw!')
      })
    };

    this._http.post(this._baseUrl + 'apiW/Data/JoinRequest', data, httpOptions
    ).subscribe((resutls: ResultModel) => {
      this.joinReqResult = resutls;
      },
      (error) => {
        this._busy = false;
        console.log(error);
      },
      () => {
        this.showMailSentModal = true;
        this._busy = false;        
        })
  }

}

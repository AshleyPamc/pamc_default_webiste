import { Component, OnInit } from '@angular/core';
import { Faq } from '../models/faq';
import { AppService } from '../Services/app.service';

@Component({
  selector: 'app-websites',
  templateUrl: './websites.component.html',
  styleUrls: ['./websites.component.css']
})
export class WebsitesComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {

    // Navigate to MediVouch section if this page was requested by home screen Pop-Up
    if (this._appService.DataService.goToMediVouchSection == true) {
      this._appService.DataService.goToMediVouchSection = false;
      //var scrollElem = document.querySelector('#MediVouchSection'); // Scrolling like this is too fast
      //scrollElem.scrollIntoView();                                  // Scrolling like this is too fast
      this.scrollToElement(document.getElementById("MediVouchSection"));
    }
    else {
      var scrollElem = document.querySelector('#top');
      scrollElem.scrollIntoView();
    }
  }

  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  }

  GetFaqForProviders() {
    let c: Faq = new Faq();
    c.usertype = "1";
    c.answer = "";
    c.question = "";
    this._appService.DataService.GetFaqs(c);
  }
  GetFaqForClients() {
    let c: Faq = new Faq();
    c.usertype = "3";
    c.answer = "";
    c.question = "";
    this._appService.DataService.GetFaqs(c);
  }
  GetFaqForBureaus() {
    let c: Faq = new Faq();
    c.usertype = "2";
    c.answer = "";
    c.question = "";
    this._appService.DataService.GetFaqs(c);
  }
}

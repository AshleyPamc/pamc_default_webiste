import { Component, OnInit } from '@angular/core';
import { AppService } from '../Services/app.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FAQComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
    
  }

}

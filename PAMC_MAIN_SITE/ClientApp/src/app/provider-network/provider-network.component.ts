import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { AppService } from '../Services/app.service';

@Component({
  selector: 'app-provider-network',
  templateUrl: './provider-network.component.html',
  styleUrls: ['./provider-network.component.css']
})
export class ProviderNetworkComponent implements OnInit {

  @Input() joinForm: FormGroup;

  submitted: boolean = false;

  validFormData: boolean = true;

  validPracNum: boolean = true;
  validPracName: boolean = true;
  validContNum: boolean = true;
  validPracEmail: boolean = true;

  

  constructor(public _appService: AppService, private fb: FormBuilder) { }

  ngOnInit() {
    var scrollElem = document.querySelector('#top');
    scrollElem.scrollIntoView();

    //this.joinForm = this.fb.group({
    //  'pracNum': ['', [Validators.required, Validators.maxLength(9999999)]],
    //  'pracName': ['', Validators.required],
    //  'contactNum': ['', Validators.required],
    //  'pracEmail': ['', Validators.required, Validators.email]
    //})

    this.joinForm = this.fb.group({
      pracNum: ['', [Validators.required]],
      pracName: ['', [Validators.required]],
      contNum: ['', [Validators.required]],
      pracEmail: ['', [Validators.required]],
    });
  }

  get pracNum() { return this.joinForm.get('pracNum'); }
  get pracName() { return this.joinForm.get('pracName'); }
  get contNum() { return this.joinForm.get('contNum'); }
  get pracEmail() { return this.joinForm.get('pracEmail'); }
  //get f() { return this.joinForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid    
    if (this.pracNum.value.trim() == "")
    {
      this.validPracNum = false;
    }
    else
    {
      (this.validPracNum = true);
    }

    if (this.pracName.value.trim() == "")
    {
      this.validPracName = false;
    }
    else
    {
      this.validPracName = true;
    }

    if (this.contNum.value.trim() == "")
    {
      this.validContNum = false;
    }
    else
    {
      this.validContNum = true;
    }

    if (this.pracEmail.value.trim() == "")
    {
      this.validPracEmail = false;
    }
    else
    {
      this.validPracEmail = true;
    }

    if (this.pracNum.value.trim() == "" || this.pracName.value.trim() == "" || this.contNum.value.trim() == "" || this.pracEmail.value.trim() == "") {
      this.validFormData = false;
      return;
    }
    else {
      this.validFormData = true;
    }

    this.SendJoinReguest();
  }

  SendJoinReguest() {
    if (this.joinForm.valid) {
      this._appService.DataService.joinDetail.provid = this.pracNum.value;
      this._appService.DataService.joinDetail.practiceName = this.pracName.value;
      this._appService.DataService.joinDetail.contactNumber = this.contNum.value;
      this._appService.DataService.joinDetail.emailAddress = this.pracEmail.value;

      this._appService.DataService.SendJoinRequest(this._appService.DataService.joinDetail);
    }
    else {
      this.submitted = true;
    }
  }

  CloseMailSentModal() {
    this._appService.DataService.showMailSentModal = false;
    this.joinForm.reset();
  }

}

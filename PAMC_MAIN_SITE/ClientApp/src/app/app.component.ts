import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './Services/app.service';
import { timer, of, Observable, merge, interval, Subscription } from 'rxjs';
import { concatMapTo, map, mapTo, repeat, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public media: boolean = false;

  public showCookieNotice: boolean = false;
  public showCookieFullInfoModal: boolean = false;
  public bShowMediVouchModal: boolean = false;
  mediVouchModalSub: Subscription;
  
  constructor(private _router: Router, public _appService: AppService) {

    // Cookie    
    let c = localStorage.getItem("PamcWebsiteCookies");
    if (c == undefined || c == null || c != "Accepted") {
      this.showCookieNotice = true;
    } else if (c == "Accepted") {
      this.showCookieNotice = false;
    }

    // Nav to Landing page
    //this._router.navigateByUrl('/Home');  // No!! Do this with routes
  }

  ngOnInit() {

    // 2 second delay to display Medivouch modal
    //this.mediVouchModalSub = interval(2000).subscribe((x => {
    //  this.ShowMediVouchModal();
    //}));
    
  }

  // Contstantly check what is the screen size
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this._appService.DataService.ScreenWidth = window.innerWidth;
  }

  ShowMediVouchModal() {
    this.bShowMediVouchModal = true;    
    this.mediVouchModalSub.unsubscribe();
  }

  GoToMediVouchSection() {
    this.bShowMediVouchModal = false; // Close MediVouch Pop-Up
    this._appService.DataService.goToMediVouchSection = true; // Tell Self-Service Portals page to go to medivouch section when it loads
    this._router.navigateByUrl('/Websites'); // Navigate to Self-Service Portals page
    
  }

  ShowCookieFullInfoModal() {
    this.showCookieFullInfoModal = true;
  }

  DismissCookieFullInfoModal() {
    this.showCookieFullInfoModal = false;
    this.HideCookieNotice();
  }

  HideCookieNotice() {
    this.showCookieNotice = false;
    localStorage.setItem("PamcWebsiteCookies", "Accepted");
  }
}

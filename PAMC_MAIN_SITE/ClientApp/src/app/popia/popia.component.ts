import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popia',
  templateUrl: './popia.component.html',
  styleUrls: ['./popia.component.css']
})
export class PopiaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var scrollElem = document.querySelector('#top');
    scrollElem.scrollIntoView();
  }

}

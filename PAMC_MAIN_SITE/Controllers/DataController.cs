﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PAMC_MAIN_SITE.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Text;

namespace PAMC_MAIN_SITE.Controllers
{
    [Route("apiW/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        [Route("GetFaqs")]
        [HttpPost]
        public List<Faq> GetFaqs([FromBody] Faq data)
        {
            List<Faq> list = new List<Faq>();

            try
            {
                var authHeader = Request.Headers["Authorization"].ToString();
                if (authHeader != null && authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
                {
                    var token = authHeader.Substring("Basic ".Length).Trim();
                    //System.Console.WriteLine(token);
                    var credentialstring = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                    var credentials = credentialstring.Split(':');
                    if (credentials[0] == "&nUwrLd6ymR#MpcTJqA2" && credentials[1] == "QsNKSL+vkx9(*ZmIbDw!")
                    {
                        using (SqlConnection cn = new SqlConnection("Server=192.168.16.13;Database=PamcPortal;User Id=PamcPortal;Password = #Portal@Pamc; Trusted_Connection=false"))
                        {
                            SqlCommand cmd = new SqlCommand();
                            if (cn.State != System.Data.ConnectionState.Open)
                            {
                                cn.Open();
                            }

                            cmd.Connection = cn;

                            DataTable dt = new DataTable();

                            cmd.Parameters.Add(new SqlParameter("USERTYPE", data.usertype));

                            cmd.CommandText = "SELECT * FROM FAQ WHERE USERTYPE = @USERTYPE AND ENABLED = 1";

                            dt.Load(cmd.ExecuteReader());

                            foreach (DataRow row in dt.Rows)
                            {
                                Faq f = new Faq();

                                f.answer = row["ANSWER"].ToString();
                                f.question = row["QUESTION"].ToString();

                                list.Add(f);
                            }

                        }
                    }
                    else
                    {
                        return new List<Faq>();
                    }
                }
                else
                {
                    return new List<Faq>();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return list;
        }

        [Route("JoinRequest")]
        [HttpPost]
        public ResultModel JoinRequest([FromBody] JoinMail data)
        {
            try
            {
                var authHeader = Request.Headers["Authorization"].ToString();
                if (authHeader != null && authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
                {
                    var token = authHeader.Substring("Basic ".Length).Trim();
                    //System.Console.WriteLine(token);
                    var credentialstring = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                    var credentials = credentialstring.Split(':');
                    if (credentials[0] == "&nUwrLd6ymR#MpcTJqA2" && credentials[1] == "QsNKSL+vkx9(*ZmIbDw!")
                    {
                        string body = "Good day\r\n\r\n" +
                          "The following provider requested through the PAMC website to join the PAMC network:\r\n\r\n" +
                          $"Practice Number: {data.provid}\r\n" +
                          $"Practice Name: {data.practiceName}\r\n" +
                          $"Contact Number: {data.contactNumber}\r\n" +
                          $"Email Address: {data.emailAddress}\r\n\r\n" +
                          "Regards\r\n" +
                          "PAMC Website";

                        DataTable dt = new DataTable();
                        using (SqlConnection cn = new SqlConnection("Server=192.168.16.13;Database=DRC;User Id=sa;Password = galnetdata; Trusted_Connection=false"))
                        {
                            SqlCommand cmd = new SqlCommand();
                            if (cn.State != System.Data.ConnectionState.Open)
                            {
                                cn.Open();
                            }

                            cmd.Connection = cn;

                            cmd.CommandText = "SELECT * FROM EDI_MAILSERVERS WHERE ROWID = 3";

                            dt.Load(cmd.ExecuteReader());
                        }

                        // Send internal mail to network@pamc.co.za
                        bool success = false;
                        if (dt.Rows[0]["SMTP"].ToString() == "True")
                        {
                            success = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(
                                dt.Rows[0]["SERVERADDRESS"].ToString(),
                                dt.Rows[0]["USERNAME"].ToString(),
                                dt.Rows[0]["PASSWORD"].ToString(),
                                dt.Rows[0]["MAILFROM"].ToString(),
                                "PAMC Website: Request to join network",
                                body,
                                "network@pamc.co.za",       //to
                                new List<FileInfo>(),       //attachement 
                                ""         //bcc not working 
                                );
                        }
                        else
                        {
                            success = PAMC.Utilities.Email.Sender.SendEmail(
                                System.Net.Mail.SmtpDeliveryMethod.Network,
                                dt.Rows[0]["MAILFROM"].ToString(),
                                dt.Rows[0]["SERVERADDRESS"].ToString(),
                                dt.Rows[0]["USERNAME"].ToString(),
                                dt.Rows[0]["PASSWORD"].ToString(),
                                "PAMC Website: Request to join network",
                                new List<FileInfo>(),
                                "network@pamc.co.za",
                                dt.Rows[0]["MAILFROM"].ToString(), body);
                        }

                        if (success)
                        {
                            // Send mail to requester (to email address entered on web form)
                            body = "Good day\r\n\r\n" +
                                      "Your request was sent with the following details:\r\n\r\n" +
                                      $"Practice Number: {data.provid}\r\n" +
                                      $"Practice Name: {data.practiceName}\r\n" +
                                      $"Contact Number: {data.contactNumber}\r\n" +
                                      $"Email Address: {data.emailAddress}\r\n\r\n" +
                                      "Regards\r\n" +
                                      "PAMC Website";


                            if (dt.Rows[0]["SMTP"].ToString() == "True")
                            {
                                success = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(
                                    dt.Rows[0]["SERVERADDRESS"].ToString(),
                                    dt.Rows[0]["USERNAME"].ToString(),
                                    dt.Rows[0]["PASSWORD"].ToString(),
                                    dt.Rows[0]["MAILFROM"].ToString(),
                                    "PAMC Website: Request to join network",
                                    body,
                                    data.emailAddress.Trim(),       //to
                                    new List<FileInfo>(),       //attachement 
                                    ""         //bcc not working 
                                    );
                            }
                            else
                            {
                                success = PAMC.Utilities.Email.Sender.SendEmail(
                                    System.Net.Mail.SmtpDeliveryMethod.Network,
                                    dt.Rows[0]["MAILFROM"].ToString(),
                                    dt.Rows[0]["SERVERADDRESS"].ToString(),
                                    dt.Rows[0]["USERNAME"].ToString(),
                                    dt.Rows[0]["PASSWORD"].ToString(),
                                    "PAMC Website: Request to join network",
                                    new List<FileInfo>(),
                                    data.emailAddress.Trim(),
                                    dt.Rows[0]["MAILFROM"].ToString(), body);
                            }

                            // Send success result even if mail could not be sent to requester
                            ResultModel rm = new ResultModel();
                            rm.success = "1";
                            rm.message = "success";
                            return rm;
                        }
                        else
                        {
                            ResultModel rm = new ResultModel();
                            rm.success = "0";
                            rm.message = "Email client retuned an error and the email was not sent. Please try again or contact our offices.";
                            return rm;
                        }
                    }
                    else
                    {
                        ResultModel rm = new ResultModel();
                        rm.success = "0";
                        rm.message = "Unauthorised";
                        return rm;
                    }
                }
                else
                {
                    ResultModel rm = new ResultModel();
                    rm.success = "0";
                    rm.message = "Unauthorised";
                    return rm;
                }
            }
            catch (Exception ex)
            {
                ResultModel rm = new ResultModel();
                rm.success = "0";
                rm.message = ex.Message;
                return rm;
            }
        }
    }
}

